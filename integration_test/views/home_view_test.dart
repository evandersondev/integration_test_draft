import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:integration_test_draft/interfaces/dog_list_repository_interface.dart';
import 'package:integration_test_draft/models/dog_model.dart';
import 'package:integration_test_draft/views/home_view.dart';
import 'package:mockito/annotations.dart';

import '../mocks/dog_list_result_mock.dart';

@GenerateMocks([], customMocks: [
  MockSpec<IDogListRepository>(as: #MockDogListRepository),
])
void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  // late IDogListRepository repository;

  setUp(() {
    // repository = MockDogListRepository();
  });

  testWidgets('HomeView integration test', (WidgetTester tester) async {
    // when(repository.list()).thenAnswer((_) async => [
    //       DogModel.fromJson(jsonDecode(dogListResultMock)),
    //     ]);

    await tester.pumpWidget(
      const MaterialApp(
        home: HomeView(),
      ),
    );

    Widget image = AspectRatio(
      aspectRatio: 16 / 9,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16.0),
        child: Image.network(
          DogModel.fromJson(jsonDecode(dogListResultMock)).url,
          fit: BoxFit.cover,
        ),
      ),
    );
    await tester.pumpAndSettle();
    Future.delayed(const Duration(seconds: 2));

    expect(find.byWidget(image), findsOneWidget);
  });
}
