import '../models/dog_model.dart';

abstract class IDogListRepository {
  Future<List<DogModel>> list();
}
