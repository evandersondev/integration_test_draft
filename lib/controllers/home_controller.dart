import 'package:flutter/foundation.dart';
import 'package:integration_test_draft/models/dog_model.dart';

import '../interfaces/dog_list_repository_interface.dart';

class HomeController {
  final IDogListRepository _repository;
  HomeController(this._repository);

  final dogsList = ValueNotifier<List<DogModel>>([]);
  final loading = ValueNotifier<bool>(false);

  Future<void> getDogs() async {
    loading.value = true;
    final dogs = await _repository.list();
    dogsList.value = dogs;
    loading.value = false;
  }
}
