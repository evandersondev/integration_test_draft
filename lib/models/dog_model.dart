class DogModel {
  final String url;

  DogModel(this.url);

  factory DogModel.fromJson(Map<String, dynamic> json) {
    return DogModel(json['message']);
  }
}
