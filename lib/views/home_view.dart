import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:integration_test_draft/models/dog_model.dart';
import 'package:integration_test_draft/repositories/dog_list_repository.dart';
import 'package:integration_test_draft/services/dog_list_service.dart';

import '../controllers/home_controller.dart';

class HomeView extends StatefulWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final _controller = HomeController(
    DogListRepository(
      DogListService(Dio()),
    ),
  );

  @override
  void initState() {
    super.initState();

    _controller.getDogs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home - Dog List'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: ValueListenableBuilder<List<DogModel>>(
            valueListenable: _controller.dogsList,
            builder: (_, dogs, __) {
              return RefreshIndicator(
                onRefresh: () => _controller.getDogs(),
                child: ListView.separated(
                  itemBuilder: (context, index) => AspectRatio(
                    aspectRatio: 16 / 9,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(16.0),
                      child: Image.network(
                        dogs[index].url,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  separatorBuilder: (context, index) => const Divider(),
                  itemCount: dogs.length,
                ),
              );
            }),
      ),
    );
  }
}
