import 'package:dio/dio.dart';

class DogListService {
  final Dio dio;

  DogListService(this.dio);

  Future<Map<String, dynamic>> getDogs() async {
    var response = await dio.get('https://dog.ceo/api/breeds/image/random');
    return response.data as Map<String, dynamic>;
  }
}
