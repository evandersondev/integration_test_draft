import 'package:integration_test_draft/models/dog_model.dart';
import 'package:integration_test_draft/services/dog_list_service.dart';

import '../interfaces/dog_list_repository_interface.dart';

class DogListRepository implements IDogListRepository {
  final DogListService service;

  DogListRepository(this.service);

  @override
  Future<List<DogModel>> list() async {
    List<DogModel> dogs = [];

    for (var i = 0; i < 10; i++) {
      final response = await service.getDogs();
      dogs.add(DogModel.fromJson(response));
    }

    return dogs;
  }
}
